package io.aurora.ams.service;

import io.aurora.ams.domain.Verification;
import io.aurora.ams.web.rest.dto.VerificationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Verification.
 */
public interface VerificationService {

    /**
     * Save a verification.
     * 
     * @param verificationDTO the entity to save
     * @return the persisted entity
     */
    VerificationDTO save(VerificationDTO verificationDTO);

    /**
     *  Get all the verifications.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Verification> findAll(Pageable pageable);

    /**
     *  Get the "id" verification.
     *  
     *  @param id the id of the entity
     *  @return the entity
     */
    VerificationDTO findOne(Long id);

    /**
     *  Delete the "id" verification.
     *  
     *  @param id the id of the entity
     */
    void delete(Long id);
}
