package io.aurora.ams.service.impl;

import io.aurora.ams.service.VerificationService;
import io.aurora.ams.domain.Verification;
import io.aurora.ams.repository.VerificationRepository;
import io.aurora.ams.web.rest.dto.VerificationDTO;
import io.aurora.ams.web.rest.mapper.VerificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Verification.
 */
@Service
@Transactional
public class VerificationServiceImpl implements VerificationService{

    private final Logger log = LoggerFactory.getLogger(VerificationServiceImpl.class);
    
    @Inject
    private VerificationRepository verificationRepository;
    
    @Inject
    private VerificationMapper verificationMapper;
    
    /**
     * Save a verification.
     * 
     * @param verificationDTO the entity to save
     * @return the persisted entity
     */
    public VerificationDTO save(VerificationDTO verificationDTO) {
        log.debug("Request to save Verification : {}", verificationDTO);
        Verification verification = verificationMapper.verificationDTOToVerification(verificationDTO);
        verification = verificationRepository.save(verification);
        VerificationDTO result = verificationMapper.verificationToVerificationDTO(verification);
        return result;
    }

    /**
     *  Get all the verifications.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<Verification> findAll(Pageable pageable) {
        log.debug("Request to get all Verifications");
        Page<Verification> result = verificationRepository.findAll(pageable); 
        return result;
    }

    /**
     *  Get one verification by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public VerificationDTO findOne(Long id) {
        log.debug("Request to get Verification : {}", id);
        Verification verification = verificationRepository.findOne(id);
        VerificationDTO verificationDTO = verificationMapper.verificationToVerificationDTO(verification);
        return verificationDTO;
    }

    /**
     *  Delete the  verification by id.
     *  
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Verification : {}", id);
        verificationRepository.delete(id);
    }
}
