package io.aurora.ams.repository;

import io.aurora.ams.domain.Verification;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Verification entity.
 */
@SuppressWarnings("unused")
public interface VerificationRepository extends JpaRepository<Verification,Long> {

}
