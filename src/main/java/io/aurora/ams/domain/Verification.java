package io.aurora.ams.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import io.aurora.ams.domain.enumeration.ContactPointSystem;

/**
 * A Verification.
 */
@Entity
@Table(name = "verification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Verification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "verification_id", nullable = false)
    private String verificationId;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "system", nullable = false)
    private ContactPointSystem system;

    @NotNull
    @Column(name = "value", nullable = false)
    private String value;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "verified_at")
    private ZonedDateTime verifiedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVerificationId() {
        return verificationId;
    }

    public void setVerificationId(String verificationId) {
        this.verificationId = verificationId;
    }

    public ContactPointSystem getSystem() {
        return system;
    }

    public void setSystem(ContactPointSystem system) {
        this.system = system;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getVerifiedAt() {
        return verifiedAt;
    }

    public void setVerifiedAt(ZonedDateTime verifiedAt) {
        this.verifiedAt = verifiedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Verification verification = (Verification) o;
        if(verification.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, verification.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Verification{" +
            "id=" + id +
            ", verificationId='" + verificationId + "'" +
            ", system='" + system + "'" +
            ", value='" + value + "'" +
            ", code='" + code + "'" +
            ", verifiedAt='" + verifiedAt + "'" +
            '}';
    }
}
