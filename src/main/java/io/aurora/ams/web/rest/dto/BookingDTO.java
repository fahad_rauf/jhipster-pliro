package io.aurora.ams.web.rest.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import io.aurora.ams.domain.enumeration.BookingState;

/**
 * A DTO for the Booking entity.
 */
public class BookingDTO implements Serializable {

    private Long id;

    @NotNull
    private String bookingCode;

    @NotNull
    private ZonedDateTime expiresAt;

    @NotNull
    private BookingState state;

    @NotNull
    private String slotId;

    @NotNull
    private String resourceId;

    @NotNull
    private String organizationId;

    @NotNull
    private String personId;

    private String appointmentId;

    private String verificationId;

    private String paymentId;

    private String notes;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }
    public ZonedDateTime getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(ZonedDateTime expiresAt) {
        this.expiresAt = expiresAt;
    }
    public BookingState getState() {
        return state;
    }

    public void setState(BookingState state) {
        this.state = state;
    }
    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }
    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }
    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }
    public String getVerificationId() {
        return verificationId;
    }

    public void setVerificationId(String verificationId) {
        this.verificationId = verificationId;
    }
    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingDTO bookingDTO = (BookingDTO) o;

        if ( ! Objects.equals(id, bookingDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "BookingDTO{" +
            "id=" + id +
            ", bookingCode='" + bookingCode + "'" +
            ", expiresAt='" + expiresAt + "'" +
            ", state='" + state + "'" +
            ", slotId='" + slotId + "'" +
            ", resourceId='" + resourceId + "'" +
            ", organizationId='" + organizationId + "'" +
            ", personId='" + personId + "'" +
            ", appointmentId='" + appointmentId + "'" +
            ", verificationId='" + verificationId + "'" +
            ", paymentId='" + paymentId + "'" +
            ", notes='" + notes + "'" +
            '}';
    }
}
