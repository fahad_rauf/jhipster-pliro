package io.aurora.ams.web.rest.mapper;

import io.aurora.ams.domain.*;
import io.aurora.ams.web.rest.dto.VerificationDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Verification and its DTO VerificationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VerificationMapper {

    VerificationDTO verificationToVerificationDTO(Verification verification);

    List<VerificationDTO> verificationsToVerificationDTOs(List<Verification> verifications);

    Verification verificationDTOToVerification(VerificationDTO verificationDTO);

    List<Verification> verificationDTOsToVerifications(List<VerificationDTO> verificationDTOs);
}
