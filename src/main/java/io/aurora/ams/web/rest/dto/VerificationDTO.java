package io.aurora.ams.web.rest.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import io.aurora.ams.domain.enumeration.ContactPointSystem;

/**
 * A DTO for the Verification entity.
 */
public class VerificationDTO implements Serializable {

    private Long id;

    @NotNull
    private String verificationId;

    @NotNull
    private ContactPointSystem system;

    @NotNull
    private String value;

    @NotNull
    private String code;

    private ZonedDateTime verifiedAt;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getVerificationId() {
        return verificationId;
    }

    public void setVerificationId(String verificationId) {
        this.verificationId = verificationId;
    }
    public ContactPointSystem getSystem() {
        return system;
    }

    public void setSystem(ContactPointSystem system) {
        this.system = system;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public ZonedDateTime getVerifiedAt() {
        return verifiedAt;
    }

    public void setVerifiedAt(ZonedDateTime verifiedAt) {
        this.verifiedAt = verifiedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VerificationDTO verificationDTO = (VerificationDTO) o;

        if ( ! Objects.equals(id, verificationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "VerificationDTO{" +
            "id=" + id +
            ", verificationId='" + verificationId + "'" +
            ", system='" + system + "'" +
            ", value='" + value + "'" +
            ", code='" + code + "'" +
            ", verifiedAt='" + verifiedAt + "'" +
            '}';
    }
}
