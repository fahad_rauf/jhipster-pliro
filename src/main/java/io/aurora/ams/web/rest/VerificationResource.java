package io.aurora.ams.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.aurora.ams.domain.Verification;
import io.aurora.ams.service.VerificationService;
import io.aurora.ams.web.rest.util.HeaderUtil;
import io.aurora.ams.web.rest.util.PaginationUtil;
import io.aurora.ams.web.rest.dto.VerificationDTO;
import io.aurora.ams.web.rest.mapper.VerificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Verification.
 */
@RestController
@RequestMapping("/api")
public class VerificationResource {

    private final Logger log = LoggerFactory.getLogger(VerificationResource.class);
        
    @Inject
    private VerificationService verificationService;
    
    @Inject
    private VerificationMapper verificationMapper;
    
    /**
     * POST  /verifications : Create a new verification.
     *
     * @param verificationDTO the verificationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new verificationDTO, or with status 400 (Bad Request) if the verification has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/verifications",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VerificationDTO> createVerification(@Valid @RequestBody VerificationDTO verificationDTO) throws URISyntaxException {
        log.debug("REST request to save Verification : {}", verificationDTO);
        if (verificationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("verification", "idexists", "A new verification cannot already have an ID")).body(null);
        }
        VerificationDTO result = verificationService.save(verificationDTO);
        return ResponseEntity.created(new URI("/api/verifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("verification", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /verifications : Updates an existing verification.
     *
     * @param verificationDTO the verificationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated verificationDTO,
     * or with status 400 (Bad Request) if the verificationDTO is not valid,
     * or with status 500 (Internal Server Error) if the verificationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/verifications",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VerificationDTO> updateVerification(@Valid @RequestBody VerificationDTO verificationDTO) throws URISyntaxException {
        log.debug("REST request to update Verification : {}", verificationDTO);
        if (verificationDTO.getId() == null) {
            return createVerification(verificationDTO);
        }
        VerificationDTO result = verificationService.save(verificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("verification", verificationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /verifications : get all the verifications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of verifications in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/verifications",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<VerificationDTO>> getAllVerifications(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Verifications");
        Page<Verification> page = verificationService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/verifications");
        return new ResponseEntity<>(verificationMapper.verificationsToVerificationDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /verifications/:id : get the "id" verification.
     *
     * @param id the id of the verificationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the verificationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/verifications/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VerificationDTO> getVerification(@PathVariable Long id) {
        log.debug("REST request to get Verification : {}", id);
        VerificationDTO verificationDTO = verificationService.findOne(id);
        return Optional.ofNullable(verificationDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /verifications/:id : delete the "id" verification.
     *
     * @param id the id of the verificationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/verifications/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVerification(@PathVariable Long id) {
        log.debug("REST request to delete Verification : {}", id);
        verificationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("verification", id.toString())).build();
    }

}
