(function() {
    'use strict';

    angular
        .module('pliroApp')
        .controller('VerificationDialogController', VerificationDialogController);

    VerificationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Verification'];

    function VerificationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Verification) {
        var vm = this;
        vm.verification = entity;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('pliroApp:verificationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.verification.id !== null) {
                Verification.update(vm.verification, onSaveSuccess, onSaveError);
            } else {
                Verification.save(vm.verification, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.verifiedAt = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }
})();
