(function() {
    'use strict';

    angular
        .module('pliroApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('verification', {
            parent: 'entity',
            url: '/verification',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Verifications'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/verification/verifications.html',
                    controller: 'VerificationController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('verification-detail', {
            parent: 'entity',
            url: '/verification/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Verification'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/verification/verification-detail.html',
                    controller: 'VerificationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Verification', function($stateParams, Verification) {
                    return Verification.get({id : $stateParams.id});
                }]
            }
        })
        .state('verification.new', {
            parent: 'verification',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/verification/verification-dialog.html',
                    controller: 'VerificationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                verificationId: null,
                                system: null,
                                value: null,
                                code: null,
                                verifiedAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('verification', null, { reload: true });
                }, function() {
                    $state.go('verification');
                });
            }]
        })
        .state('verification.edit', {
            parent: 'verification',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/verification/verification-dialog.html',
                    controller: 'VerificationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Verification', function(Verification) {
                            return Verification.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('verification', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('verification.delete', {
            parent: 'verification',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/verification/verification-delete-dialog.html',
                    controller: 'VerificationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Verification', function(Verification) {
                            return Verification.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('verification', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
