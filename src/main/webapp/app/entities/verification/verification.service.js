(function() {
    'use strict';
    angular
        .module('pliroApp')
        .factory('Verification', Verification);

    Verification.$inject = ['$resource', 'DateUtils'];

    function Verification ($resource, DateUtils) {
        var resourceUrl =  'api/verifications/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.verifiedAt = DateUtils.convertDateTimeFromServer(data.verifiedAt);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
