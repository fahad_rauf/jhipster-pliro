(function() {
    'use strict';

    angular
        .module('pliroApp')
        .controller('VerificationDeleteController',VerificationDeleteController);

    VerificationDeleteController.$inject = ['$uibModalInstance', 'entity', 'Verification'];

    function VerificationDeleteController($uibModalInstance, entity, Verification) {
        var vm = this;
        vm.verification = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Verification.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
