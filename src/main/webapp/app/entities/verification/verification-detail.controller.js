(function() {
    'use strict';

    angular
        .module('pliroApp')
        .controller('VerificationDetailController', VerificationDetailController);

    VerificationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Verification'];

    function VerificationDetailController($scope, $rootScope, $stateParams, entity, Verification) {
        var vm = this;
        vm.verification = entity;
        
        var unsubscribe = $rootScope.$on('pliroApp:verificationUpdate', function(event, result) {
            vm.verification = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
