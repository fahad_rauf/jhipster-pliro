package io.aurora.ams.web.rest;

import io.aurora.ams.PliroApp;
import io.aurora.ams.domain.Verification;
import io.aurora.ams.repository.VerificationRepository;
import io.aurora.ams.service.VerificationService;
import io.aurora.ams.web.rest.dto.VerificationDTO;
import io.aurora.ams.web.rest.mapper.VerificationMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.aurora.ams.domain.enumeration.ContactPointSystem;

/**
 * Test class for the VerificationResource REST controller.
 *
 * @see VerificationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PliroApp.class)
@WebAppConfiguration
@IntegrationTest
public class VerificationResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_VERIFICATION_ID = "AAAAA";
    private static final String UPDATED_VERIFICATION_ID = "BBBBB";

    private static final ContactPointSystem DEFAULT_SYSTEM = ContactPointSystem.PHONE;
    private static final ContactPointSystem UPDATED_SYSTEM = ContactPointSystem.FAX;
    private static final String DEFAULT_VALUE = "AAAAA";
    private static final String UPDATED_VALUE = "BBBBB";
    private static final String DEFAULT_CODE = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";

    private static final ZonedDateTime DEFAULT_VERIFIED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_VERIFIED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_VERIFIED_AT_STR = dateTimeFormatter.format(DEFAULT_VERIFIED_AT);

    @Inject
    private VerificationRepository verificationRepository;

    @Inject
    private VerificationMapper verificationMapper;

    @Inject
    private VerificationService verificationService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restVerificationMockMvc;

    private Verification verification;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VerificationResource verificationResource = new VerificationResource();
        ReflectionTestUtils.setField(verificationResource, "verificationService", verificationService);
        ReflectionTestUtils.setField(verificationResource, "verificationMapper", verificationMapper);
        this.restVerificationMockMvc = MockMvcBuilders.standaloneSetup(verificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        verification = new Verification();
        verification.setVerificationId(DEFAULT_VERIFICATION_ID);
        verification.setSystem(DEFAULT_SYSTEM);
        verification.setValue(DEFAULT_VALUE);
        verification.setCode(DEFAULT_CODE);
        verification.setVerifiedAt(DEFAULT_VERIFIED_AT);
    }

    @Test
    @Transactional
    public void createVerification() throws Exception {
        int databaseSizeBeforeCreate = verificationRepository.findAll().size();

        // Create the Verification
        VerificationDTO verificationDTO = verificationMapper.verificationToVerificationDTO(verification);

        restVerificationMockMvc.perform(post("/api/verifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(verificationDTO)))
                .andExpect(status().isCreated());

        // Validate the Verification in the database
        List<Verification> verifications = verificationRepository.findAll();
        assertThat(verifications).hasSize(databaseSizeBeforeCreate + 1);
        Verification testVerification = verifications.get(verifications.size() - 1);
        assertThat(testVerification.getVerificationId()).isEqualTo(DEFAULT_VERIFICATION_ID);
        assertThat(testVerification.getSystem()).isEqualTo(DEFAULT_SYSTEM);
        assertThat(testVerification.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testVerification.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testVerification.getVerifiedAt()).isEqualTo(DEFAULT_VERIFIED_AT);
    }

    @Test
    @Transactional
    public void checkVerificationIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = verificationRepository.findAll().size();
        // set the field null
        verification.setVerificationId(null);

        // Create the Verification, which fails.
        VerificationDTO verificationDTO = verificationMapper.verificationToVerificationDTO(verification);

        restVerificationMockMvc.perform(post("/api/verifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(verificationDTO)))
                .andExpect(status().isBadRequest());

        List<Verification> verifications = verificationRepository.findAll();
        assertThat(verifications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSystemIsRequired() throws Exception {
        int databaseSizeBeforeTest = verificationRepository.findAll().size();
        // set the field null
        verification.setSystem(null);

        // Create the Verification, which fails.
        VerificationDTO verificationDTO = verificationMapper.verificationToVerificationDTO(verification);

        restVerificationMockMvc.perform(post("/api/verifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(verificationDTO)))
                .andExpect(status().isBadRequest());

        List<Verification> verifications = verificationRepository.findAll();
        assertThat(verifications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = verificationRepository.findAll().size();
        // set the field null
        verification.setValue(null);

        // Create the Verification, which fails.
        VerificationDTO verificationDTO = verificationMapper.verificationToVerificationDTO(verification);

        restVerificationMockMvc.perform(post("/api/verifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(verificationDTO)))
                .andExpect(status().isBadRequest());

        List<Verification> verifications = verificationRepository.findAll();
        assertThat(verifications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = verificationRepository.findAll().size();
        // set the field null
        verification.setCode(null);

        // Create the Verification, which fails.
        VerificationDTO verificationDTO = verificationMapper.verificationToVerificationDTO(verification);

        restVerificationMockMvc.perform(post("/api/verifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(verificationDTO)))
                .andExpect(status().isBadRequest());

        List<Verification> verifications = verificationRepository.findAll();
        assertThat(verifications).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVerifications() throws Exception {
        // Initialize the database
        verificationRepository.saveAndFlush(verification);

        // Get all the verifications
        restVerificationMockMvc.perform(get("/api/verifications?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(verification.getId().intValue())))
                .andExpect(jsonPath("$.[*].verificationId").value(hasItem(DEFAULT_VERIFICATION_ID.toString())))
                .andExpect(jsonPath("$.[*].system").value(hasItem(DEFAULT_SYSTEM.toString())))
                .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.toString())))
                .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
                .andExpect(jsonPath("$.[*].verifiedAt").value(hasItem(DEFAULT_VERIFIED_AT_STR)));
    }

    @Test
    @Transactional
    public void getVerification() throws Exception {
        // Initialize the database
        verificationRepository.saveAndFlush(verification);

        // Get the verification
        restVerificationMockMvc.perform(get("/api/verifications/{id}", verification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(verification.getId().intValue()))
            .andExpect(jsonPath("$.verificationId").value(DEFAULT_VERIFICATION_ID.toString()))
            .andExpect(jsonPath("$.system").value(DEFAULT_SYSTEM.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.verifiedAt").value(DEFAULT_VERIFIED_AT_STR));
    }

    @Test
    @Transactional
    public void getNonExistingVerification() throws Exception {
        // Get the verification
        restVerificationMockMvc.perform(get("/api/verifications/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVerification() throws Exception {
        // Initialize the database
        verificationRepository.saveAndFlush(verification);
        int databaseSizeBeforeUpdate = verificationRepository.findAll().size();

        // Update the verification
        Verification updatedVerification = new Verification();
        updatedVerification.setId(verification.getId());
        updatedVerification.setVerificationId(UPDATED_VERIFICATION_ID);
        updatedVerification.setSystem(UPDATED_SYSTEM);
        updatedVerification.setValue(UPDATED_VALUE);
        updatedVerification.setCode(UPDATED_CODE);
        updatedVerification.setVerifiedAt(UPDATED_VERIFIED_AT);
        VerificationDTO verificationDTO = verificationMapper.verificationToVerificationDTO(updatedVerification);

        restVerificationMockMvc.perform(put("/api/verifications")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(verificationDTO)))
                .andExpect(status().isOk());

        // Validate the Verification in the database
        List<Verification> verifications = verificationRepository.findAll();
        assertThat(verifications).hasSize(databaseSizeBeforeUpdate);
        Verification testVerification = verifications.get(verifications.size() - 1);
        assertThat(testVerification.getVerificationId()).isEqualTo(UPDATED_VERIFICATION_ID);
        assertThat(testVerification.getSystem()).isEqualTo(UPDATED_SYSTEM);
        assertThat(testVerification.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testVerification.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testVerification.getVerifiedAt()).isEqualTo(UPDATED_VERIFIED_AT);
    }

    @Test
    @Transactional
    public void deleteVerification() throws Exception {
        // Initialize the database
        verificationRepository.saveAndFlush(verification);
        int databaseSizeBeforeDelete = verificationRepository.findAll().size();

        // Get the verification
        restVerificationMockMvc.perform(delete("/api/verifications/{id}", verification.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Verification> verifications = verificationRepository.findAll();
        assertThat(verifications).hasSize(databaseSizeBeforeDelete - 1);
    }
}
