package io.aurora.ams.web.rest;

import io.aurora.ams.PliroApp;
import io.aurora.ams.domain.Booking;
import io.aurora.ams.repository.BookingRepository;
import io.aurora.ams.service.BookingService;
import io.aurora.ams.web.rest.dto.BookingDTO;
import io.aurora.ams.web.rest.mapper.BookingMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.aurora.ams.domain.enumeration.BookingState;

/**
 * Test class for the BookingResource REST controller.
 *
 * @see BookingResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PliroApp.class)
@WebAppConfiguration
@IntegrationTest
public class BookingResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_BOOKING_CODE = "AAAAA";
    private static final String UPDATED_BOOKING_CODE = "BBBBB";

    private static final ZonedDateTime DEFAULT_EXPIRES_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_EXPIRES_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_EXPIRES_AT_STR = dateTimeFormatter.format(DEFAULT_EXPIRES_AT);

    private static final BookingState DEFAULT_STATE = BookingState.AWAITING_VERIFICATION;
    private static final BookingState UPDATED_STATE = BookingState.VERIFICATION_EXPIRED;
    private static final String DEFAULT_SLOT_ID = "AAAAA";
    private static final String UPDATED_SLOT_ID = "BBBBB";
    private static final String DEFAULT_RESOURCE_ID = "AAAAA";
    private static final String UPDATED_RESOURCE_ID = "BBBBB";
    private static final String DEFAULT_ORGANIZATION_ID = "AAAAA";
    private static final String UPDATED_ORGANIZATION_ID = "BBBBB";
    private static final String DEFAULT_PERSON_ID = "AAAAA";
    private static final String UPDATED_PERSON_ID = "BBBBB";
    private static final String DEFAULT_APPOINTMENT_ID = "AAAAA";
    private static final String UPDATED_APPOINTMENT_ID = "BBBBB";
    private static final String DEFAULT_VERIFICATION_ID = "AAAAA";
    private static final String UPDATED_VERIFICATION_ID = "BBBBB";
    private static final String DEFAULT_PAYMENT_ID = "AAAAA";
    private static final String UPDATED_PAYMENT_ID = "BBBBB";
    private static final String DEFAULT_NOTES = "AAAAA";
    private static final String UPDATED_NOTES = "BBBBB";

    @Inject
    private BookingRepository bookingRepository;

    @Inject
    private BookingMapper bookingMapper;

    @Inject
    private BookingService bookingService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restBookingMockMvc;

    private Booking booking;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BookingResource bookingResource = new BookingResource();
        ReflectionTestUtils.setField(bookingResource, "bookingService", bookingService);
        ReflectionTestUtils.setField(bookingResource, "bookingMapper", bookingMapper);
        this.restBookingMockMvc = MockMvcBuilders.standaloneSetup(bookingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        booking = new Booking();
        booking.setBookingCode(DEFAULT_BOOKING_CODE);
        booking.setExpiresAt(DEFAULT_EXPIRES_AT);
        booking.setState(DEFAULT_STATE);
        booking.setSlotId(DEFAULT_SLOT_ID);
        booking.setResourceId(DEFAULT_RESOURCE_ID);
        booking.setOrganizationId(DEFAULT_ORGANIZATION_ID);
        booking.setPersonId(DEFAULT_PERSON_ID);
        booking.setAppointmentId(DEFAULT_APPOINTMENT_ID);
        booking.setVerificationId(DEFAULT_VERIFICATION_ID);
        booking.setPaymentId(DEFAULT_PAYMENT_ID);
        booking.setNotes(DEFAULT_NOTES);
    }

    @Test
    @Transactional
    public void createBooking() throws Exception {
        int databaseSizeBeforeCreate = bookingRepository.findAll().size();

        // Create the Booking
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isCreated());

        // Validate the Booking in the database
        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeCreate + 1);
        Booking testBooking = bookings.get(bookings.size() - 1);
        assertThat(testBooking.getBookingCode()).isEqualTo(DEFAULT_BOOKING_CODE);
        assertThat(testBooking.getExpiresAt()).isEqualTo(DEFAULT_EXPIRES_AT);
        assertThat(testBooking.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testBooking.getSlotId()).isEqualTo(DEFAULT_SLOT_ID);
        assertThat(testBooking.getResourceId()).isEqualTo(DEFAULT_RESOURCE_ID);
        assertThat(testBooking.getOrganizationId()).isEqualTo(DEFAULT_ORGANIZATION_ID);
        assertThat(testBooking.getPersonId()).isEqualTo(DEFAULT_PERSON_ID);
        assertThat(testBooking.getAppointmentId()).isEqualTo(DEFAULT_APPOINTMENT_ID);
        assertThat(testBooking.getVerificationId()).isEqualTo(DEFAULT_VERIFICATION_ID);
        assertThat(testBooking.getPaymentId()).isEqualTo(DEFAULT_PAYMENT_ID);
        assertThat(testBooking.getNotes()).isEqualTo(DEFAULT_NOTES);
    }

    @Test
    @Transactional
    public void checkBookingCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setBookingCode(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isBadRequest());

        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkExpiresAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setExpiresAt(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isBadRequest());

        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setState(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isBadRequest());

        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSlotIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setSlotId(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isBadRequest());

        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkResourceIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setResourceId(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isBadRequest());

        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOrganizationIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setOrganizationId(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isBadRequest());

        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPersonIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setPersonId(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(booking);

        restBookingMockMvc.perform(post("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isBadRequest());

        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBookings() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);

        // Get all the bookings
        restBookingMockMvc.perform(get("/api/bookings?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(booking.getId().intValue())))
                .andExpect(jsonPath("$.[*].bookingCode").value(hasItem(DEFAULT_BOOKING_CODE.toString())))
                .andExpect(jsonPath("$.[*].expiresAt").value(hasItem(DEFAULT_EXPIRES_AT_STR)))
                .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
                .andExpect(jsonPath("$.[*].slotId").value(hasItem(DEFAULT_SLOT_ID.toString())))
                .andExpect(jsonPath("$.[*].resourceId").value(hasItem(DEFAULT_RESOURCE_ID.toString())))
                .andExpect(jsonPath("$.[*].organizationId").value(hasItem(DEFAULT_ORGANIZATION_ID.toString())))
                .andExpect(jsonPath("$.[*].personId").value(hasItem(DEFAULT_PERSON_ID.toString())))
                .andExpect(jsonPath("$.[*].appointmentId").value(hasItem(DEFAULT_APPOINTMENT_ID.toString())))
                .andExpect(jsonPath("$.[*].verificationId").value(hasItem(DEFAULT_VERIFICATION_ID.toString())))
                .andExpect(jsonPath("$.[*].paymentId").value(hasItem(DEFAULT_PAYMENT_ID.toString())))
                .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())));
    }

    @Test
    @Transactional
    public void getBooking() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);

        // Get the booking
        restBookingMockMvc.perform(get("/api/bookings/{id}", booking.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(booking.getId().intValue()))
            .andExpect(jsonPath("$.bookingCode").value(DEFAULT_BOOKING_CODE.toString()))
            .andExpect(jsonPath("$.expiresAt").value(DEFAULT_EXPIRES_AT_STR))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.slotId").value(DEFAULT_SLOT_ID.toString()))
            .andExpect(jsonPath("$.resourceId").value(DEFAULT_RESOURCE_ID.toString()))
            .andExpect(jsonPath("$.organizationId").value(DEFAULT_ORGANIZATION_ID.toString()))
            .andExpect(jsonPath("$.personId").value(DEFAULT_PERSON_ID.toString()))
            .andExpect(jsonPath("$.appointmentId").value(DEFAULT_APPOINTMENT_ID.toString()))
            .andExpect(jsonPath("$.verificationId").value(DEFAULT_VERIFICATION_ID.toString()))
            .andExpect(jsonPath("$.paymentId").value(DEFAULT_PAYMENT_ID.toString()))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBooking() throws Exception {
        // Get the booking
        restBookingMockMvc.perform(get("/api/bookings/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBooking() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);
        int databaseSizeBeforeUpdate = bookingRepository.findAll().size();

        // Update the booking
        Booking updatedBooking = new Booking();
        updatedBooking.setId(booking.getId());
        updatedBooking.setBookingCode(UPDATED_BOOKING_CODE);
        updatedBooking.setExpiresAt(UPDATED_EXPIRES_AT);
        updatedBooking.setState(UPDATED_STATE);
        updatedBooking.setSlotId(UPDATED_SLOT_ID);
        updatedBooking.setResourceId(UPDATED_RESOURCE_ID);
        updatedBooking.setOrganizationId(UPDATED_ORGANIZATION_ID);
        updatedBooking.setPersonId(UPDATED_PERSON_ID);
        updatedBooking.setAppointmentId(UPDATED_APPOINTMENT_ID);
        updatedBooking.setVerificationId(UPDATED_VERIFICATION_ID);
        updatedBooking.setPaymentId(UPDATED_PAYMENT_ID);
        updatedBooking.setNotes(UPDATED_NOTES);
        BookingDTO bookingDTO = bookingMapper.bookingToBookingDTO(updatedBooking);

        restBookingMockMvc.perform(put("/api/bookings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
                .andExpect(status().isOk());

        // Validate the Booking in the database
        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeUpdate);
        Booking testBooking = bookings.get(bookings.size() - 1);
        assertThat(testBooking.getBookingCode()).isEqualTo(UPDATED_BOOKING_CODE);
        assertThat(testBooking.getExpiresAt()).isEqualTo(UPDATED_EXPIRES_AT);
        assertThat(testBooking.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testBooking.getSlotId()).isEqualTo(UPDATED_SLOT_ID);
        assertThat(testBooking.getResourceId()).isEqualTo(UPDATED_RESOURCE_ID);
        assertThat(testBooking.getOrganizationId()).isEqualTo(UPDATED_ORGANIZATION_ID);
        assertThat(testBooking.getPersonId()).isEqualTo(UPDATED_PERSON_ID);
        assertThat(testBooking.getAppointmentId()).isEqualTo(UPDATED_APPOINTMENT_ID);
        assertThat(testBooking.getVerificationId()).isEqualTo(UPDATED_VERIFICATION_ID);
        assertThat(testBooking.getPaymentId()).isEqualTo(UPDATED_PAYMENT_ID);
        assertThat(testBooking.getNotes()).isEqualTo(UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void deleteBooking() throws Exception {
        // Initialize the database
        bookingRepository.saveAndFlush(booking);
        int databaseSizeBeforeDelete = bookingRepository.findAll().size();

        // Get the booking
        restBookingMockMvc.perform(delete("/api/bookings/{id}", booking.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Booking> bookings = bookingRepository.findAll();
        assertThat(bookings).hasSize(databaseSizeBeforeDelete - 1);
    }
}
